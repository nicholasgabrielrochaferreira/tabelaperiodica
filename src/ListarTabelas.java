
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JList;

public class ListarTabelas {
    public void listar(JList lista){
        try {
            lista.removeAll();
            

            DefaultListModel dlm = new DefaultListModel();
            Connection c = Conexao.obterConexao();
            String SQL = "select * from sc_tabela_periodica.tabela";
            
            //PreparedStatement ps = c.prepareStatement(SQL);
            Statement s = c.createStatement();
            //ResultSet rs = ps.executeQuery(SQL);
            ResultSet rs = s.executeQuery(SQL);

            while (rs.next()) {
                dlm.addElement(rs.getString("titulo"));
            }

            lista.setModel(dlm);
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(ListarTabelas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
