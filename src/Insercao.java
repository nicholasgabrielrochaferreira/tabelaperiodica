import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Insercao {

    public void Inserir(String nome_e, String simbolo, String numero_atomico, String massa_atomica) {

        
        try {
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("insert into sc_tabela_periodica.elemento_quimico(nome_e, simbolo, numero_atomico, massa_atomica) values(?, ?, ?, ?)");
            
            ps.setString(1, nome_e);
            ps.setString(2, simbolo);
            ps.setString(3, numero_atomico);
            ps.setString(4, massa_atomica);

            ps.executeUpdate();
            c.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
}
