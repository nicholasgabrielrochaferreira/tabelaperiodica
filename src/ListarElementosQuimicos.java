
import javax.swing.JList;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;

public class ListarElementosQuimicos {

    public void listar(JList listaTabelaPeriodica) {

        try {
            listaTabelaPeriodica.removeAll();
            

            DefaultListModel dlm = new DefaultListModel();
            Connection c = Conexao.obterConexao();
            String SQL = "select * from sc_tabela_periodica.elemento_quimico";      

            //PreparedStatement ps = c.prepareStatement(SQL);
            Statement s = c.createStatement();
            //ResultSet rs = ps.executeQuery(SQL);
            ResultSet rs = s.executeQuery(SQL);

            while (rs.next()) {
                dlm.addElement(rs.getString("nome_e"));
            }

            listaTabelaPeriodica.setModel(dlm);
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(ListarElementosQuimicos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
