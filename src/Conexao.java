import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {

    public static Connection obterConexao() {

        try {
            
            Class.forName("org.postgresql.Driver");

            String usuario = "nicholas";
            String senha = "mtab80";
            String banco = "jdbc:postgresql://127.0.0.1/test007";
            return DriverManager.getConnection(banco, usuario, senha);

        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
}
