
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Insercao2 {

    public void inserir(String titulo, String ano_edicao, String autor, String base_massas) {
        try {
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("insert into sc_tabela_periodica.tabela(titulo, ano_edicao, autor, base_massas) values(?, ?, ?, ?)");
            
            ps.setString(1, titulo);
            ps.setString(2, ano_edicao);
            ps.setString(3, autor);
            ps.setString(4, base_massas);
            
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
